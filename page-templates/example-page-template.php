<?php
/**
 * Template page that displays a form and publishes in content
 * 
 * disabled.Template Name : Example lliurament form
 *
 * name and text found in includes/include-pagetemplate-TEMPLATENAME.php
 *
 * This class defines all code necessary to run during the plugin's activation
 * 
 *
 * @link       https://fotografiamatematica.cat
 * @since      1.0.0
 * @package    fotomates-wp-plugin
 * @subpackage fotomates-wp-plugin/includes
 * @author     Jorge - vitrubio.net <jorge@vitrubio.net>
 */


// Help URI: http://www.wpexplorer.com/wordpress-page-templates-plugin/
// Original Author: WPExplorer


// check this for block theme
// https://fullsiteediting.com/lessons/how-to-use-php-templates-in-block-themes/
// https://developer.wordpress.org/reference/functions/block_template_part/

?>
<?php get_header(); //the old wordpress header call?>

<?php 
  // create the bloc in the hedader and assign them to a variable
  //     $block_part = do_blocks( '<!-- wp:WHATEVER-PART-HERE { ..., "theme":"your-theme-slug"}/-->' ); 
  // using it: echo the variable where you want the block
  //     echo $block_part

//$block_content_header = do_blocks( '
//    <!-- wp:template-part {"slug":"header","area":"header","tagName":"header", "theme":"fotomates-wp-blocks-theme"} /-->
//' );
//$block_content_footer = do_blocks( '
//    <!-- wp:template-part {"slug":"footer","area":"footer","tagName":"footer", "theme":"fotomates-wp-blocks-theme"} /-->
//' );
//$block_content_main_open = do_blocks ('
//<!-- wp:group {"tagName":"main"} -->
//<main class="wp-block-group">
//  <!-- wp:group {"layout":{"type":"constrained"}} -->
//  <div class="wp-block-group">
//    <!-- wp:spacer {"height":"var:preset|spacing|50"} -->
//    <div style="height:var(--wp--preset--spacing--50)" aria-hidden="true" class="wp-block-spacer"></div>
//    <!-- /wp:spacer -->
// 
//    <!-- wp:post-title {"textAlign":"center","level":1} /-->
// 
//    <!-- wp:spacer {"height":"var:preset|spacing|30","style":{"spacing":{"margin":{"top":"0","bottom":"0"}}}} -->
//    <div style="margin-top:0;margin-bottom:0;height:var(--wp--preset--spacing--30)" aria-hidden="true"
//      class="wp-block-spacer"></div>
//    <!-- /wp:spacer -->
// 
//    <!-- wp:post-featured-image {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|40"}}}} /-->
//  </div>
//  <!-- /wp:group -->
//</main>
//<!-- /wp:group -->
//');
//
//$block_content_main_close = do_blocks ('
//');
?>
<?php //echo $block_content_header; ?>
<?php //echo $block_content_main_open; ?>
<?php //echo $block_content_main_close; ?>

  <header id="title" class="page-title">
  <?php the_title();?>
  </header>
  <main id="content" class="">
   <?php the_content();?>
  </main>
<?php get_footer(); //the old wordpress footer call ?>
