<?php

/*
*
* @package            fotomates-wp-plugin
* @author             jorge-vitrubio.net
* @link               https://fotografiamatematica.cat
* @since              1.0.0
* @wordpress-plugin
*
* Plugin Name:        Fotografia Matematica wp plugin
* Plugin URI:         https://gitlab.com/jorge-vitrubio/fotomates-wp-plugin
* Description:        Different needs for the Fotografia Matemática 2024 Wordpress theme needs. Adds support for: SVG.
* Tested up to:       6.5
* Requires at least:  6.1
* Requires PHP:       7.4 
* Date:               2024 04 22
* Version:            1.6.0
* Author:             jorge - vitrubio.net
* Author URI:         https://vitrubio.net/
* License:            GPL 3.0
* License URI:        https://www.gnu.org/licenses/gpl-3.0.html
* Text Domain:        fotomates-wpplugin-textdomain
* Domain Path:        /languages/
*/

/*
if ever read this never forget to check
howto write a pluggin by Wordpress.org
https://codex.wordpress.org/Writing_a_Plugin
and the best practices
https://developer.wordpress.org/plugins/plugin-basics/best-practices/
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
//defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'FOTOMATES_WPPLUGIN_VERSION', '1.0.0' );

/* *
* define Plugin path
* https://developer.wordpress.org/reference/functions/plugin_dir_path/#comment-1113
* * * * * * * * * * * * * * * * * * */

define( 'FOTOMATES_WPPLUGIN_FILE', __FILE__ );

define( 'FOTOMATES_WPPLUGIN_PATH', plugin_dir_path( __FILE__ ) ); //in server

define( 'FOTOMATES_WPPLUGIN_URL', plugin_dir_url( __FILE__ ) ); //public

//define( 'FOTOMATES_WPPLUGIN_BASENAME', plugin_basename( __FILE__ ) );

include( FOTOMATES_WPPLUGIN_PATH . 'includes/plugin-init-textdomain.php');

include( FOTOMATES_WPPLUGIN_PATH . 'includes/plugin-settings-pannel.php');

include( FOTOMATES_WPPLUGIN_PATH . 'includes/enable-svg.php');

include( FOTOMATES_WPPLUGIN_PATH . 'includes/stylesheet-public.php');

include( FOTOMATES_WPPLUGIN_PATH . 'includes/custom-post-type-lliuraments.php');

include( FOTOMATES_WPPLUGIN_PATH . 'includes/lliurament-content/lliurament-content.php');

include( FOTOMATES_WPPLUGIN_PATH . 'includes/StarRatingField.php');

//include( FOTOMATES_WPPLUGIN_PATH . 'includes/custom-meta-fields-lliuraments.php');

//include( FOTOMATES_WPPLUGIN_PATH . 'includes/example-include-pagetemplate.php');
