��    A      $  Y   ,      �      �     �     �     �      �  #        ?     V     k     }     �     �  )   �     �     �  6   �     )     1     A     U     r  "   �  -   �     �     �     �  
   	          (     F     R     d     z     �     �     �     �     �  2   �  '   %	     M	     e	     l	     �	  3   �	     �	     �	     �	      
  '   
  #   6
     Z
     i
     u
     �
     �
     �
     �
     �
     �
          (     >     W  w  l      �  #     &   )     P      d  #   �     �     �     �  +   �     $     5  7   O     �     �  6   �     �     �     �          $  #   5  -   Y     �     �     �  
   �     �  )   �               +     A     X     j     }     �     �  2   �  '   �     #     ;      B     c  B   ~     �     �  "   �     	  '   $     L     k     �     �     �     �     �     �     �          1  #   K     o     �     "               =       :           A                  @      	         >                 0   &          -   ?      6                $   5       %                 #   ;          +   7       (   !          <   ,                     4              .   '   8   1                            2   3   /   )   9   *   
             Actualiza el tipus de lliurament Add New lliurament tag Add or remove lliurament tag Affegeix lliurament Affegeix nou tipus de lliurament Affegeix o treu tipus de lliurament Affegeix un lliurament All lliuraments tags Allow Half Rating Allow half ratings? Cerca lliurament Cerca tipus de lliurament Choose from the most used lliurament tags Clear Color Control de les opcions de la web Fotografia Matematica Default Desa lliurament Edit lliurament tag Edita el tipus de lliurament Edita lliurament Error! Please enter a higher value Escull entre els tipus de lliurament mes usat Gestion d'usuarias List (fa-awesome) List (unstyled) Lliurament Lliurament etiqueta Lliurament tag Not Lliurament Lliuraments Lliuraments Tipus Lliuraments etiquetes Lliuraments mare/pare: Lliuraments tipus Maximum Rating Maximum number of stars Mostra el lliurament New lliurament tag No hem pogut trovar ningún lliurament lliurament  No hem trovat cap lliurament a la brosa Nou tipus de lliurament Number Parent lliurament tag Parent lliurament tag: Please enter a value clicking on stars form 1 to %s Return Type Reusable blocks Search lliurament tags Select theme? Separa el tipus de lliurament amb comes Separate lliurament tag with commas Settings Saved Star Rating The value is too large! Theme Tipus de lliurament Tipus pare de lliurament Tipus pare de lliurament: Tipus que no siguin Lliurament Tots els lliuraments Tots tipus de lliuraments Update lliurament tag What should be returned? fotomates-lliurament Project-Id-Version: FotoMates Plugin
PO-Revision-Date: 2024-04-16 12:34+0200
Last-Translator: 
Language-Team: https://gitlab.com/jorge-vitrubio/fotomates-wp-plugin
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.2
X-Poedit-Basepath: ../..
X-Poedit-KeywordsList: __;_e;_x
X-Poedit-SearchPath-0: .
 Actualiza el tipus de lliurament Afegeix nova etiqueta de lliurament Afegeix o treu etiquetes de lliurament Affegeix lliurament Affegeix nou tipus de lliurament Affegeix o treu tipus de lliurament Affegeix un lliurament Totes les etiquetes Lliuraments Permet mitja valoració Voleu permetre la meitat a les puntuacions? Cerca lliurament Cerca tipus de lliurament Tria entre les etiquetes de lliurament més utilitzades Neteja Color Control de les opcions de la web Fotografia Matematica Per defecte Desa lliurament Edita la etiqueta de lliurament Edita el tipus de lliurament Edita lliurament Error! Introduïu un valor més alt Escull entre els tipus de lliurament mes usat Gestion d'usuarias Llista (fa-awesome) Llista (sense estils) Lliurament Lliurament etiqueta L'etiqueta de lliurament No es Lliurament Lliuraments Lliuraments Tipus Lliuraments etiquetes Lliuraments mare/pare: Lliuraments tipus Puntuació màxima Nombre màxim d'estrelles Mostra el lliurament Nova etiqueta de lliurament No hem pogut trovar ningún lliurament lliurament  No hem trovat cap lliurament a la brosa Nou tipus de lliurament Nombre Etiqueta pare/mare de Lliurament Lliuramente etiqueta pare: Introduïu un valor en fer clic a les estrelles des de 1 fins a %s Tipus de retorn Blocs reutilitzables Cerca les etiquetes de lliuraments Voleu seleccionar el tema? Separa el tipus de lliurament amb comes Separe les etiquetes amb comes Configuració desada Valoració de estrelles El valor és massa gran! Tema Tipus de lliurament Tipus pare de lliurament Tipus pare de lliurament: Tipus que no siguin Lliurament Tots els lliuraments Tots tipus de lliuraments Actualitza l'etiqueta de lliurament Què s'ha de retornar? fotomates-lliurament 