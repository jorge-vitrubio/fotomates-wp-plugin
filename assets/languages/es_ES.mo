��    A      $  Y   ,      �      �     �     �     �      �  #        ?     V     k     }     �     �  )   �     �     �  6   �     )     1     A     U     r  "   �  -   �     �     �     �  
   	          (     F     R     d     z     �     �     �     �     �  2   �  '   %	     M	     e	     l	     �	  3   �	     �	     �	     �	      
  '   
  #   6
     Z
     i
     u
     �
     �
     �
     �
     �
     �
          (     >     W  z  l     �  $     #   )     M     \      |     �     �     �  '   �          !  5   =     s     {  9   �     �     �     �     �       +     1   F     x     �     �     �     �  $   �     �     �     
          2     A     U     r     �  *   �  0   �     �               3  P   N     �     �     �     �  %   �  )        G     Y     o     �     �     �     �     �     �     �          9     R     "               =       :           A                  @      	         >                 0   &          -   ?      6                $   5       %                 #   ;          +   7       (   !          <   ,                     4              .   '   8   1                            2   3   /   )   9   *   
             Actualiza el tipus de lliurament Add New lliurament tag Add or remove lliurament tag Affegeix lliurament Affegeix nou tipus de lliurament Affegeix o treu tipus de lliurament Affegeix un lliurament All lliuraments tags Allow Half Rating Allow half ratings? Cerca lliurament Cerca tipus de lliurament Choose from the most used lliurament tags Clear Color Control de les opcions de la web Fotografia Matematica Default Desa lliurament Edit lliurament tag Edita el tipus de lliurament Edita lliurament Error! Please enter a higher value Escull entre els tipus de lliurament mes usat Gestion d'usuarias List (fa-awesome) List (unstyled) Lliurament Lliurament etiqueta Lliurament tag Not Lliurament Lliuraments Lliuraments Tipus Lliuraments etiquetes Lliuraments mare/pare: Lliuraments tipus Maximum Rating Maximum number of stars Mostra el lliurament New lliurament tag No hem pogut trovar ningún lliurament lliurament  No hem trovat cap lliurament a la brosa Nou tipus de lliurament Number Parent lliurament tag Parent lliurament tag: Please enter a value clicking on stars form 1 to %s Return Type Reusable blocks Search lliurament tags Select theme? Separa el tipus de lliurament amb comes Separate lliurament tag with commas Settings Saved Star Rating The value is too large! Theme Tipus de lliurament Tipus pare de lliurament Tipus pare de lliurament: Tipus que no siguin Lliurament Tots els lliuraments Tots tipus de lliuraments Update lliurament tag What should be returned? fotomates-lliurament Project-Id-Version: FotoMates Plugin
PO-Revision-Date: 2024-04-16 12:42+0200
Last-Translator: 
Language-Team: https://gitlab.com/jorge-vitrubio/fotomates-wp-plugin
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.2
X-Poedit-Basepath: ../..
X-Poedit-KeywordsList: __;_e;_x
X-Poedit-SearchPath-0: .
 Actualiza el tipo de entrega Añade una nueva etiqueta de entrega Añade o borra etiquetas de entrega Añade entrega Añade un nuevo tipo de entrega Añade o quita tipos de entregas Añade una entrega Todas las etiquetas de entregas Permitir media puntuación ¿Quieres permitir medias puntuaciones? Busca entrega Busca los tipos de entregas Escoge entre las etiquetas de entrega más utilizadas Limpiar Color Control de las opciones de la web Fotografía Matemática Por defecto Guarda entrega Edita la etiqueta de entrega Edita el tipo de entrega Edita entrega ¡Error! Por favor introduce un valor mayor Escoge entre los tipos de entrega más utilizados Gestión de usuarias Lista (fa-awesome) Lista (sin estilos) Entrega Entrega etiqueta La etiqueta de entrega NO es entrega Entregas Entregas Tipos Entregas etiquetas Entraga madre/padre: Entregas tipos Puntuación máxima Número máximo de estrellas Muestra la entrega Nueva etiqueta de entrega No hemos podido encontrar ninguna entrega  No hemos encontrado ninguna entrega en la basura Nuevo tipo de entrega Número Etiqueta padre de entrega Etiqueta padre de entrega: Por favor, introduce un valor haciendo clic en las estrellas desde la 1 hasta %s Tipo de retorno Bloques reutilizables Busca etiquetas de entrega ¿Seleccionar el tema? Separa los tipos de entrega con comas Separa las etiquetas de entrega con comas Ajustes guardados Puntuación estrellas ¡El valor es demasiado alto! Tema Tipos de entrega Tipos padre de entrega Tipos padre de entrega: Tipos que no son entregas Todas las entregas Todos los tipos de entregas Actualiza etiqueta de entrega ¿Que debería devolver? fotomates-lliurament 