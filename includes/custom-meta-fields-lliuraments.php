<?php

/**
 * Add custom post metabox with custom fields
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @link       https://fotografiamatematica.cat
 * @since      1.0.0
 * @package    fotomates-wp-plugin
 * @subpackage fotomates-wp-plugin/includes
 * @author     Jorge - vitrubio.net <jorge@vitrubio.net>
 */      


// add the metabox
// https://developer.wordpress.org/reference/functions/add_meta_box/
//if ( ! function_exists('fotomates_lliurament_add_meta_box') ){
//  function fotomates_lliurament_add_meta_box() {
//    add_meta_box(
//    'custom_post_metabox',
//    __('Detalls dels lliuraments'),
//    'lliurament_display_meta_box',
//    'fotomates-lliurament',
//    'advanced',
//    'default',
//    );
//  }
//};

// https://developer.wordpress.org/reference/hooks/add_meta_boxes/
//add_action( 'add_meta_boxes_fotomates-lliurament', 'fotomates_lliurament_add_meta_box' );

// display content in the metabox
// https://wpengine.com/resources/create-custom-wordpress-meta-box/
//if ( ! function_exists('lliurament_display_meta_box') ){
//  function lliurament_display_meta_box( $post ){
//    wp_nonce_field( plugin_basename(__FILE__), 'lliurament_nonce_field' );
//    $html  = '<p class="description"> Aqui es mostra la foto lliurada, pots canviarla si vols</p>'; 
//    $html .= '<label id="lliurament-file-foto" for="lliurament-file-foto">Foto lliurada </label>';
//    $html .= '<input type="file" id="lliurament-file-foto" name="lliurament-file-foto" value="" />';
//    //$html .= '<input type="file" id="lliurament-file-foto" name="lliurament-file-foto" value="' . get_post_meta( get_the_ID(), 'lliurament_file_foto', true ) . '" placeholder="fitxer foto lliurade" />';
//
//    //$filearray = get_post_meta( $post->ID, 'lliurament_file_foto', true);
//    //$this_lliurament_file_foto = $filearray['url'];
//
//    //if ( $this_lliurament_file_foto != '' ){
//    //  $html .= '<div><p>Fitxer foto actual: ' . $this_lliurament_file_foto . '</p></div>';
//    //};
//    $html .= '<p> La foto es: <em>' . get_post_meta( $post->ID, 'lliurament_file_foto', true ) . '</em></p>';
//
//    echo $html; // this echoes a single stream of html
//  }
//};

// saving data
//if ( ! function_exists('fotomates_lliurament_save_meta_box_data') ){
//  function fotomates_lliurament_save_meta_box_data( $post_id ) {
//    if ( fotomates_lliurament_user_can_save( $post_id, 'fotomates_lliurament_nonce_field' ) ) {
//      if ( isset( $_POST['lliurament_file_foto'] ) && 0 < count( strlen( trim( $_POST['lliurament_file_foto'] ) ) ) ) {
//        $file_foto_lleurade = stripslashes( strip_tags( $_POST['lliurament_file_foto'] ) );
//        update_post_meta( $post_id, 'lliurament_file_foto', $file_foto_lleurade  );
//      }
//    }
//  }
//}
//add_action( 'save_post', 'fotomates_lliurament_save_meta_box_data' );
//
//if ( ! function_exists('fotomates_lliurament_display_foto') ){
//  function fotomates_lliurament_display_foto( $content ) {
//    if ( is_single () ){
//      $html = 'La foto lleurade es: ' . get_post_meta( get_the_ID(), 'lliurament_file_foto', true );
//      $content .= $html;
//    }
//    return $content;
//  }
//}
//add_action('the_content', 'fotomates_lliurament_display_foto');
//
//if ( ! function_exists('fotomates_lliurament_user_can_save') ){
//  function fotomates_lliurament_user_can_save( $post_id, $nonce ) {
//    $is_autosave = wp_is_post_autosave( $post_id );
//    $is_revision = wp_is_post_revision( $post_id );
//    $is_valid_nonce = ( isset( $_POST[ $nonce ] ) && wp_verify_nonce( $_POST [ $nonce ], plugin_basename( __FILE__ ) ) );
//    return ! ( $is_autosave || $is_revision ) && $is_valid_nonce;
//  }
//}
