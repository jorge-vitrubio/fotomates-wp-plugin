<?php

/**
 * Add mimetypes support: svg
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @link       https://fotografiamatematica.cat
 * @since      1.0.0
 * @package    fotomates-wp-plugin
 * @subpackage fotomates-wp-plugin/includes
 * @author     Jorge - vitrubio.net <jorge@vitrubio.net>
 */      
         

 function enable_mime_types( $mimes ) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
 }
 add_filter('upload_mimes', 'enable_mime_types');
