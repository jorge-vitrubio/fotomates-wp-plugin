<?php
/**
 * Load translation, if it exists
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @link       https://fotografiamatematica.cat
 * @since      1.0.0
 * @package    fotomates-wp-plugin
 * @subpackage fotomates-wp-plugin/includes
 * @author     Jorge - vitrubio.net <jorge@vitrubio.net>
 */

function fotomates_wpplugin_init_textdomain() {
 load_plugin_textdomain( 'fotomates-wpplugin-textdomain', null, plugin_dir_path( __FILE__ ).'/assets/languages/' );
}
add_action('plugins_loaded', 'fotomates_wpplugin_init_textdomain');
