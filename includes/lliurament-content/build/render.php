<?php
/**
 * @see https://github.com/WordPress/gutenberg/blob/trunk/docs/reference-guides/block-api/block-metadata.md#render
 */
?>
<p <?php //echo get_block_wrapper_attributes(); ?>>
	<?php //esc_html_e( 'Fotomates Lliurament Content – hello from a dynamic block!', 'fotomates-lliurament-content' ); ?>
</p>
<?php
/**
 * Lliurament content Block template.
 *
 * @param array $block The block settings and attributes.
 *
 * https://developer.wordpress.org/block-editor/getting-started/tutorial/
 **/
?>
<?php
  // this content is protected in the template with swpm-partial-protection plugin
  // if placed anywere else please... protect it from public publishing
?>
<?php
// load values and assign defaults
$concursant      = get_field('lliurament_nom_concursant');
$centre          = get_field('lliurament_nom_centre_educatiu');
$mailresponsable = get_field('lliurament_email_responsable');
$mailcentre      = get_field('lliurament_email_centre');
$tipus           = get_field('lliurament_tipus_tags');
$arxiusfields    = get_field('lliurament_fotosarxiusfitxers');
if ( have_rows('lliurament_fotosarxiusfitxers') ){
  while( have_rows('lliurament_fotosarxiusfitxers') ): the_row();
    $file_foto       = get_sub_field('lliurament_file_foto');
    $file_ggb        = get_sub_field('lliurament_file_fotogeogebra');
    $file_activitat  = get_sub_field('lliurament_fitxer_activitatsdidacticas');
  endwhile;
};
?>

<?php
//echo '<p>'; var_dump($concursant      ); echo '</p>'; 
//echo '<p>'; var_dump($centre          ); echo '</p>'; 
//echo '<p>'; var_dump($mailresponsable ); echo '</p>'; 
//echo '<p>'; var_dump($mailcentre      ); echo '</p>'; 
//echo '<p>'; var_dump($tipus           ); echo '</p>'; 
//echo '<p>'; var_dump($arxiusfields    ); echo '</p>'; 
//echo '<p>'; var_dump($file_foto       ); echo '</p>'; 
//echo '<p>'; var_dump($file_ggb        ); echo '</p>'; 
//echo '<p>'; var_dump($file_activitat  ); echo '</p>'; 
?>
<section id="lliurament-contingut" class=" is-layout-constrained lliurament-content lliurament-contingut">

    <?php if ($file_foto): ?>
     <p>
      <a href="<?php echo acf_esc_html( $file_foto['url'] );?>">
        <?php echo acf_esc_html( $file_foto['filename'] );?>
      </a>
      <a href="<?php echo acf_esc_html( $file_foto['url'] );?>">
        <figure class="lliurament-file-foto">
          <?php echo wp_get_attachment_image( $file_foto['ID'], 'full' ); ?>
        </figure>
      </a>
     </p>
    <?php endif; ?>

    <?php if ($file_ggb): ?>
     <p>
      <a href="<?php echo acf_esc_html( $file_ggb['filename'] );?>" title="<?php echo acf_esc_html( $file_ggb['filename'] );?>">
        <?php echo acf_esc_html( $file_ggb['filename'] );?>
        <?php echo ( '<img class="file-icon" src="' . $file_ggb['icon'] . '" />'); ?>
        <?php echo size_format( $file_ggb['filesize'], $decimals = 2 );?>
      </a>
     </p>
    <?php endif; ?>

    <?php if ($file_activitat): ?>
     <p>
      <a href="<?php echo acf_esc_html( $file_activitat['filename'] );?>" title="<?php echo acf_esc_html( $file_activitat['filename'] );?>">
        <?php echo acf_esc_html( $file_activitat['filename'] );?>
        <?php echo ( '<img class="file-icon" src="' . $file_activitat['icon'] . '" />'); ?>
        <?php echo size_format( $file_activitat['filesize'], $decimals = 2 );?>
      </a>
     </p>
    <?php endif; ?>

  <?php
    // Detect plugin. For use on Front End only.
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    // check for plugin using plugin name
    if ( is_plugin_active( 'wp-postratings/wp-postratings.php' ) ) {
    //plugin is activated do
  ?>
  <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
  <?php };?>

</section>
