<?php
/**
 * Custom post type Lliuraments.
 *
 * This class defines all code necessary to run during the plugin's activation
 * 
 *
 * @link       https://fotografiamatematica.cat
 * @since      1.0.0
 * @package    fotomates-wp-plugin
 * @subpackage fotomates-wp-plugin/includes
 * @author     Jorge - vitrubio.net <jorge@vitrubio.net>
 */


/**
 * Start custom post types
 * ----------------------------------------------------------------------------
 * https://developer.wordpress.org/plugins/post-types/registering-custom-post-types/
 * https://developer.wordpress.org/reference/functions/register_post_type/
 */


 // Register Custom Post Type
if ( ! function_exists('fotomates_plugin_custom_post_lliurament_init') ){
  function fotomates_plugin_custom_post_lliurament_init() {
    $args = array(
 	  	'label'               => __( 'fotomates-lliurament', 'text_domain' ),
      'description'         => __( 'Lliuraments', 'text_domain' ),
      'labels'              => array(
 	  	  'name'                => _x( 'Lliuraments', 'Post Tipu General Name', 'text_domain' ),
 	  	  'singular_name'       => _x( 'Lliurament', 'Post Tipu Singular Name', 'text_domain' ),
 	  	  'menu_name'           => __( 'Lliuraments', 'text_domain' ),
 	  	  'parent_item_colon'   => __( 'Lliuraments mare/pare:', 'text_domain' ),
 	      'all_items'           => __( 'Tots els lliuraments', 'text_domain' ),
 	  	  'view_item'           => __( 'Mostra el lliurament', 'text_domain' ),
 	  	  'add_new_item'        => __( 'Affegeix un lliurament', 'text_domain' ),
 	  	  'add_new'             => __( 'Affegeix lliurament', 'text_domain' ),
 	  	  'edit_item'           => __( 'Edita lliurament', 'text_domain' ),
 	  	  'update_item'         => __( 'Desa lliurament', 'text_domain' ),
 	  	  'search_items'        => __( 'Cerca lliurament', 'text_domain' ),
 	  	  'not_lliurament'           => __( 'No hem pogut trovar ningún lliurament lliurament ', 'text_domain' ),
 	  	  'not_lliurament_in_trash'  => __( 'No hem trovat cap lliurament a la brosa', 'text_domain' )
      ),
      'supports'            => array(
        'title', 
        'editor',
        'author',
        'thumbnail',
        //'excerpt',
        //'trackbacks', 
        'custom-fields',
        'comments', 
        'revisions', 
        'page-attributes' 
      ),
      'taxonomies'          => array(
        'fotomates-lliurament-category',
        'fotomates-lliurament-seleccio',
        'fotomates-lliurament-tag'
      ),
 	  	'hierarchical'        => true,
 	  	'public'              => true,
 	  	'show_ui'             => true,
 	  	'show_in_menu'        => true,
 	  	'show_in_nav_menus'   => true,
 	  	'show_in_admin_bar'   => true,
      'show_in_rest'        => true,
 	  	'menu_position'       => 5,
 	  	'menu_icon'           => 'dashicons-book',
 	  	'can_export'          => true,
 	  	'has_archive'         => true,
 	  	'exclude_from_search' => false,
 	  	'publicly_queryable'  => true,
      'capability_type'     => 'post',
      'query_var'           => true,
      'rewrite'             =>  array(
        'slug'                => 'lliuraments'
      )
      // https://wordpress.stackexchange.com/questions/108338/capabilities-and-custom-post-types#108375
	    // https://developer.wordpress.org/reference/functions/register_post_type/#capability_type
	  	//'capabilities' 				=> array(
	    //  'read' 					          => 'read_lliurament',
	    //  'publish_posts' 			    => 'publish_lliurament',
	    //  'edit_posts'					    => 'edit_lliurament',
	    //  'edit_published_posts'    => 'edit_published_lliurament', 
	    //  'edit_others_posts' 	    => 'edit_others_lliurament',
	    //  'delete_posts' 				    => 'delete_lliurament',
	    //  'delete_published_posts'	=> 'delete_published_lliurament',
	    //  'delete_others_posts'     => 'delete_others_lliuraments',
	    //  'delete_private_posts'    => 'delete_private_lliuraments'
	    //),
	  	//'map_meta_cap' 				=> true //neded to apply the new capabilities.
 	  );
 	  register_post_type( 'fotomates-lliurament', $args );
  }
}// end function_exists fotomates_plugin_custom_post_lliurament_init

// Hook into the 'init' action
add_action( 'init', 'fotomates_plugin_custom_post_lliurament_init', 0 );


// flush rewrite and when changing theme or plugin
// https://developer.wordpress.org/reference/functions/register_post_type/#flushing-rewrite-on-activation
if ( ! function_exists( 'fotomates_lliurament_rewrite_flush') ){
  function fotomates_lliurament_rewrite_flush() {
     fotomates_plugin_custom_post_lliurament_init();
     flush_rewrite_rules();
  }
}// end function_exists fotomates_lliurament_rewrite_flush
add_action( 'after_switch_theme', 'fotomates_lliurament_rewrite_flush' );

  // give capabilities once the custom post is activated
  //if ( ! function_exists( 'fotomates_plugin_add_caps' )){
  //  function fotomates_plugin_add_caps() {
  //      //gets the XX role
  //      $admins = get_role( '' );
  //      $admins->add_cap(  );
  //      gets the administrator role
  //  }
  //}
  //add_action( 'admin_init', 'fotomates_plugin_add_caps');



/**
 * Start custom taxonomies
 * ----------------------------------------------------------------------------
 * https://developer.wordpress.org/plugins/taxonomies/working-with-custom-taxonomies/
 * https://developer.wordpress.org/reference/functions/register_taxonomy/
 */

 // Register Custom Taxonomy Category for Lliuraments
if ( ! function_exists( 'fotomates_plugin_lliurament_category_register') ){
 function fotomates_plugin_lliurament_category_register() {

 	$args = array(
 		'labels'              => array(
 	  	'name'                       => _x( 'Lliuraments tipus', 'Taxonomy General Name', 'text_domain' ),
 	  	'singular_name'              => _x( 'Tipus de lliurament', 'Taxonomy Singular Name', 'text_domain' ),
 	  	'menu_name'                  => __( 'Lliuraments Tipus', 'text_domain' ),
 	  	'all_items'                  => __( 'Tots tipus de lliuraments', 'text_domain' ),
 	  	'parent_item'                => __( 'Tipus pare de lliurament', 'text_domain' ),
 	  	'parent_item_colon'          => __( 'Tipus pare de lliurament:', 'text_domain' ),
 	  	'new_item_name'              => __( 'Nou tipus de lliurament', 'text_domain' ),
 	  	'add_new_item'               => __( 'Affegeix nou tipus de lliurament', 'text_domain' ),
 	  	'edit_item'                  => __( 'Edita el tipus de lliurament', 'text_domain' ),
 	  	'update_item'                => __( 'Actualiza el tipus de lliurament', 'text_domain' ),
 	  	'separate_items_with_commas' => __( 'Separa el tipus de lliurament amb comes', 'text_domain' ),
 	  	'search_items'               => __( 'Cerca tipus de lliurament', 'text_domain' ),
 	  	'add_or_remove_items'        => __( 'Affegeix o treu tipus de lliurament', 'text_domain' ),
 	  	'choose_from_most_used'      => __( 'Escull entre els tipus de lliurament mes usat', 'text_domain' ),
 	  	'not_lliurament'                  => __( 'Tipus que no siguin Lliurament', 'text_domain' )
    ),
 		'hierarchical'        => true,
 		//'public'              => true,
 		'show_ui'             => true,
 		'show_admin_column'   => true,
 		//'show_in_nav_menus'   => true,
    //'show_tagcloud'       => false,
    'show_in_rest'        => true,
    'query_var'           => true,
    'rewrite'             => array(
 		  'slug'                       => 'lliurament-tipus'
 	  )
  	//'capabilities' 				=> array(
    //	'manage_terms'				=> 'Fotomates Manage lliuraments Tipus',
    //	'edit_terms'				  => 'Fotomates Edit lliurament Tipus',
    //	'delete_terms'				=> 'Fotomates Delete lliuraments Tipus',
    //	'assign_terms'				=> 'Fotomates Assign lliuraments Tipus',
    //),
  	//'map_meta_cap' 				=> true //neded to apply the new capabilities.

 	);
  register_taxonomy( 'fotomates-lliurament-category', array( 'fotomates-lliurament' ) , $args );
 }
}
// Hook into the 'init' action
add_action( 'init', 'fotomates_plugin_lliurament_category_register', 0 );

// Register Custom Taxonomy Seleccio for Lliuraments
if ( ! function_exists( 'fotomates_plugin_lliurament_seleccio_register') ){
 function fotomates_plugin_lliurament_seleccio_register() {

 	$args = array(
 		'labels'              => array(
 	  	'name'                       => _x( 'Lliuraments seleccio', 'Taxonomy General Name', 'text_domain' ),
 	  	'singular_name'              => _x( 'Seleccio de lliurament', 'Taxonomy Singular Name', 'text_domain' ),
 	  	'menu_name'                  => __( 'Lliuraments Seleccionats', 'text_domain' ),
 	  	'all_items'                  => __( 'Tots lliuraments seleccionats', 'text_domain' ),
 	  	'parent_item'                => __( 'Seleccio pare de lliurament', 'text_domain' ),
 	  	'parent_item_colon'          => __( 'Seleccio pare de lliurament:', 'text_domain' ),
 	  	'new_item_name'              => __( 'Nova seleccio de lliurament', 'text_domain' ),
 	  	'add_new_item'               => __( 'Affegeix nova seleccio de lliurament', 'text_domain' ),
 	  	'edit_item'                  => __( 'Edita la seleccio de lliurament', 'text_domain' ),
 	  	'update_item'                => __( 'Actualiza la seleccio de lliurament', 'text_domain' ),
 	  	'separate_items_with_commas' => __( 'Separa el tipus de lliurament amb comes', 'text_domain' ),
 	  	'search_items'               => __( 'Cerca seleccions de lliurament', 'text_domain' ),
 	  	'add_or_remove_items'        => __( 'Affegeix o treu seleccions de lliurament', 'text_domain' ),
 	  	'choose_from_most_used'      => __( 'Escull entre les seleccions de lliurament mes usat', 'text_domain' ),
 	  	'not_lliurament'                  => __( 'Seleccions que no siguin Lliurament', 'text_domain' )
    ),
 		'hierarchical'        => true,
 		//'public'              => true,
 		//'show_ui'             => true,
 		'show_admin_column'   => true,
 		'show_in_nav_menus'   => true,
    //'show_tagcloud'       => false,
    'show_in_rest'        => true,
    'query_var'           => true,
    'rewrite'             => array(
 		  'slug'                       => 'lliurament-seleccio'
 	  )
  	//'capabilities' 				=> array(
    //	'manage_terms'				=> 'Fotomates Manage lliuraments Seleccio',
    //	'edit_terms'				  => 'Fotomates Edit lliurament Seleccio',
    //	'delete_terms'				=> 'Fotomates Delete lliuraments Seleccio',
    //	'assign_terms'				=> 'Fotomates Assign lliuraments Seleccio',
    //),
  	//'map_meta_cap' 				=> true //neded to apply the new capabilities.

 	);
  register_taxonomy( 'fotomates-lliurament-seleccio', array( 'fotomates-lliurament' ) , $args );
 }
}
// Hook into the 'init' action
add_action( 'init', 'fotomates_plugin_lliurament_seleccio_register', 0 );


// Register Custom Taxonomy Tag for Lliuraments
if ( ! function_exists( 'fotomates_plugin_lliurament_tag_register' ) ){
 function fotomates_plugin_lliurament_tag_register() {
  $args = array(
    'labels'                     => array(
      'name'                       => _x( 'Lliuraments etiquetes', 'Taxonomy General Name', 'text_domain' ),
      'singular_name'              => _x( 'Lliurament etiqueta', 'Taxonomy Singular Name', 'text_domain' ),
      'menu_name'                  => __( 'Lliuraments etiquetes', 'text_domain' ),
      'all_items'                  => __( 'All lliuraments tags', 'text_domain' ),
      'parent_item'                => __( 'Parent lliurament tag', 'text_domain' ),
      'parent_item_colon'          => __( 'Parent lliurament tag:', 'text_domain' ),
      'new_item_name'              => __( 'New lliurament tag', 'text_domain' ),
      'add_new_item'               => __( 'Add New lliurament tag', 'text_domain' ),
      'edit_item'                  => __( 'Edit lliurament tag', 'text_domain' ),
      'update_item'                => __( 'Update lliurament tag', 'text_domain' ),
      'separate_items_with_commas' => __( 'Separate lliurament tag with commas', 'text_domain' ),
      'search_items'               => __( 'Search lliurament tags', 'text_domain' ),
      'add_or_remove_items'        => __( 'Add or remove lliurament tag', 'text_domain' ),
      'choose_from_most_used'      => __( 'Choose from the most used lliurament tags', 'text_domain' ),
      'not_lliurament'             => __( 'Lliurament tag Not Lliurament', 'text_domain' )
    ),
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    //'show_in_menu'               => true,
    'show_admin_column'          => true,
    //'show_in_nav_menus'          => true,
    //'show_in_admin_column'       => false, //default false
    'show_in_rest'        => true,
    'show_tagcloud'              => true,
    'query_var'                  => true,
    'rewrite'                    => array(
      'slug'                       => 'lliurament-etiqueta',
      'with_front'								 => true
    )
		//'capabilities' 				=> array(
  	//	'manage_terms'				=> 'Fotomates Manage lliuraments Tag',
  	//	'edit_terms'				  => 'Fotomates Edit lliurament Tag',
  	//	'delete_terms'				=> 'Fotomates Delete lliuraments Tag',
  	//	'assign_terms'				=> 'Fotomates Assign lliuraments Tag',
	  //),
		//'map_meta_cap' 				=> true //neded to apply the new capabilities.
  );
  register_taxonomy( 'fotomates-lliurament-tag', array( 'fotomates-lliurament' ) , $args );
 }
}
// Hook into the 'init' action
add_action( 'init', 'fotomates_plugin_lliurament_tag_register', 0 );


 // changing the permalink
 // https://wordpress.stackexchange.com/questions/108642/permalinks-custom-post-type-custom-taxonomy-post
 // change in register_post_type the slug 'rewrite' to this
 // $rewrite = array(
 //   'slug'                       => 'lliuraments/%fotomates-lliurament-category%',
 //   'with_front'                 => true,
 //   'hierarchical'               => true,
 // );
 // uncomment below
 //
 //if ( ! function_exists('fotomates_plugin_lliurament_and_category_permalink') ){
 //  function fotomates_plugin_lliurament_and_category_permalink( $post_link, $id = 0 ){
 //      $post = get_post($id);
 //      if ( is_object( $post ) && $post->post_type == 'fotomates-lliurament' ){
 //          $terms = wp_get_object_terms( $post->ID, 'fotomates-lliurament-category' );
 //          if( $terms ){
 //              return str_replace( '%fotomates-lliurament-category%' , $terms[0]->slug , $post_link );
 //          }
 //      }
 //      return $post_ink;
 //  }
 //}
//add_filter( 'post_type_link', 'fotomates_plugin_lliurament_and_category_permalink', 1, 2 );

// Show CPT Lliurament in users posts count
// @link https://wordpress.stackexchange.com/a/348637

// @link https://wordpress.stackexchange.com/questions/152494/make-a-custom-column-sortable-by-custom-post-count
//
// Filter content of sortable column prior to user listing
// var to save value order: 'order_user_lliuraments'
//
function fotomates_lliuraments_pre_user_query( $query ) {
    global $wpdb, $current_screen;

    // Only filter in the admin
    if ( ! is_admin() )
        return;

    // Only filter on the users screen
    if ( ! ( isset( $current_screen ) && 'users' == $current_screen->id ) )
        return;

    // Only filter if orderby is set to 'lliuraments'
    if ( isset( $query->query_vars ) && isset( $query->query_vars[ 'orderby' ] )
        && ( 'order_user_lliuraments' == $query->query_vars[ 'orderby' ] ) ) {

        // We need the order - default is ASC
        $order = isset( $query->query_vars ) && isset( $query->query_vars[ 'order' ] ) && strcasecmp( $query->query_vars[ 'order' ], 'desc' ) == 0 ? 'DESC' : 'ASC';

        // Order the posts by product count
        $query->query_orderby = "ORDER BY ( SELECT COUNT(*) FROM {$wpdb->posts} order_user_lliuraments WHERE order_user_lliuraments.post_type = 'fotomates-lliurament' AND order_user_lliuraments.post_status = 'publish' AND order_user_lliuraments.post_author = {$wpdb->users}.ID ) {$order}";

    }

}

// Display sortable columns
// make column sortable
// @link https://developer.wordpress.org/reference/hooks/manage_this-screen-id_sortable_columns/
function users_lliurament_sortable_columns( $cols ) {
   //name of the column   = what value to order 
   $cols['user_lliuraments'] = 'order_user_lliuraments';
   return $cols;
}

// Add Lliurament Column
// define the column
//
function users_lliuraments_columns( $cols ) {
  $cols['user_lliuraments'] = 'Lliuraments';
  return $cols;
}

// Print Lliurament Column Value
//
function users_lliuraments_column_value( $value, $cols, $id ) {
  if( $cols == 'user_lliuraments' ) {
    global $wpdb;
    $count = (int) $wpdb->get_var( $wpdb->prepare(
      "SELECT COUNT(ID) FROM $wpdb->posts WHERE
       post_type = 'fotomates-lliurament' AND post_status = 'publish' AND post_author = %d",
       $id
    ) );
    $link = '<a href="' . admin_url() . 'edit.php?post_type=fotomates-lliurament' . '&author=' . $id . '" title="Lliuraments fets">' . $count . '</a>';
    $return = ($count > 0) ? $link : $count;
    return $return;

  }
}

// register the column function
add_action( 'pre_user_query', 'fotomates_lliuraments_pre_user_query', 1 );
add_filter( 'manage_users_sortable_columns', 'users_lliurament_sortable_columns' );
add_filter( 'manage_users_custom_column', 'users_lliuraments_column_value', 10, 3 );
add_filter( 'manage_users_columns', 'users_lliuraments_columns' );




