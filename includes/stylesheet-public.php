<?php

/**
 * Registers public stylesheet for a custom plugin.
 *
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @link       https://fotografiamatematica.cat
 * @since      1.0.0
 * @package    fotomates-wp-plugin
 * @subpackage fotomates-wp-plugin/includes
 * @author     Jorge - vitrubio.net <jorge@vitrubio.net>
 */      

// https://developer.wordpress.org/themes/basics/including-css-javascript/#stylesheets

if ( ! function_exists('fotomates_public_styles') ){
  function fotomates_public_styles() {
    wp_enqueue_style( 
      'fotomates-plugin-styles.css', // stylesheet name
      FOTOMATES_WPPLUGIN_URL . 'assets/css/fotomates-plugin-styles.css' // path to folder
    );
  }
	add_action( 'wp_enqueue_scripts', 'fotomates_public_styles' );
}
