<?php
/**      
 * Custom settings page
 *       
 * This class defines all code necessary to run during the plugin's activation.
 *       
 * @link       https://fotografiamatematica.cat
 * @since      1.0.0
 * @package    fotomates-wp-plugin
 * @subpackage fotomates-wp-plugin/includes
 * @author     Jorge - vitrubio.net <jorge@vitrubio.net>
 *
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 * https://developer.wordpress.org/plugins/settings/custom-settings-page/
 */

/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */

/**
 * custom option and settings
 */
function fotomates_settings_init() {
	// Register a new setting for "fotomates" page.
	register_setting( 'fotomates', 'fotomates_options' );

	// Register a new section in the "fotomates" page.
	add_settings_section(
		'fotomates_section_options',
		__( 'Control de les opcions de la web Fotografia Matematica', 'fotomates' ), 'fotomates_section_options_callback',
		'fotomates'
	);

	// Register a new field in the "fotomates_section_options" section, inside the "fotomates" page.
	add_settings_field(
		'fotomates_field_reusableblocks', // As of WP 4.6 this value is used only internally.
		                        // Use $args' label_for to populate the id inside the callback.
			__( 'Reusable blocks', 'fotomates' ),
		'fotomates_field_reusableblocks_cb',
		'fotomates',
		'fotomates_section_options',
		array(
			'label_for'         => 'fotomates_field_reusableblocks',
			'class'             => 'fotomates_row',
			'fotomates_custom_data' => 'custom',
		)
	);
	// Register a new field in the "fotomates_section_options" section, inside the "fotomates" page.
	add_settings_field(
		'fotomates_field_simplemembership', // As of WP 4.6 this value is used only internally.
		                        // Use $args' label_for to populate the id inside the callback.
			__( 'Gestion d\'usuarias', 'fotomates' ),
		'fotomates_field_simplemembership_cb',
		'fotomates',
		'fotomates_section_options',
		array(
			'label_for'         => 'fotomates_field_simplemembership',
			'class'             => 'fotomates_row',
			'fotomates_custom_data' => 'custom',
		)
	);
}

/**
 * Register our fotomates_settings_init to the admin_init action hook.
 */
add_action( 'admin_init', 'fotomates_settings_init' );


/**
 * Custom option and settings:
 *  - callback functions
 */


/**
 * Fotografia Matematica section callback function.
 *
 * @param array $args  The settings array, defining title, id, callback.
 */
function fotomates_section_options_callback( $args ) {
	?>
	<p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Aquestes son las opcions de la pagina Fotografia Matematica 2023. Pots modificar i pots seguir els links dels botons.', 'fotomates' ); ?></p>
	<?php
}

/**
 * Reusable Blocks field callbakc function.
 *
 * WordPress has magic interaction with the following keys: label_for, class.
 * - the "label_for" key value is used for the "for" attribute of the <label>.
 * - the "class" key value is used for the "class" attribute of the <tr> containing the field.
 * Note: you can add custom key value pairs to be used inside your callbacks.
 *
 * @param array $args
 */
function fotomates_field_reusableblocks_cb( $args ) {
	// Get the value of the setting we've registered with register_setting()
	$options = get_option( 'fotomates_options' );
	?>
	<p class="">
    <a href="<?php echo admin_url(); ?>edit.php?post_type=wp_block" class="button">Reusable Blocks</a>
    <?php esc_html_e( 'Si vols modificar els blocks reutilizables (els de color violeta) de las págines i entrades, fes click al botó.', 'fotomates' ); ?>
	</p>
	<?php
}

function fotomates_field_simplemembership_cb( $args ) {
	// Get the value of the setting we've registered with register_setting()
	$options = get_option( 'fotomates_options' );
	?>
	<p class="">
    <a href="<?php echo admin_url(); ?>admin.php?page=simple_wp_membership" class="button">Gestiona Usuarias</a>

		Es necessari fer servir <a href="https://wordpress.org/plugins/simple-membership/">SimpleMembership</a> per la gestió d'usiarias amb capacitat de pujar imatges.</p>
<p>
per affegir usuarias fem servir el plugin SimpleMembership i ens permet donar d'alta i baixa als centres.
</p>
<p>
	<?php esc_html_e( '', 'fotomates' ); ?>
	</p>
	<?php
}

/**
* top level menu
* https://developer.wordpress.org/reference/functions/add_menu_page/
*/
if( ! function_exists('fotomates_options_page') ){
  function fotomates_options_page() {
    // add top level menu page
    add_menu_page(
      $page_title = 'Fotografia Matematica plugin options', // $page_title
      $menu_title = 'FotoMates', //$menu_title
      $capability = 'manage_options', //'edit_others_posts', // $capability
      $menu_slug  = 'fotomates-options', // $menu_slug
      $function   = 'fotomates_options_page_html', //$function
			$icon_url   = 'dashicons-sos', // choose icon https://developer.wordpress.org/resource/dashicons/#menu
      $position = '25'// $position
    );
  }
}


/**
 * Register our fotomates_options_page to the admin_menu action hook.
 */
add_action( 'admin_menu', 'fotomates_options_page' );


/**
 * Top level menu callback function
 */
function fotomates_options_page_html() {
	// check user capabilities
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}

	// add error/update messages

	// check if the user have submitted the settings
	// WordPress will add the "settings-updated" $_GET parameter to the url
	if ( isset( $_GET['settings-updated'] ) ) {
		// add settings saved message with the class of "updated"
		add_settings_error( 'fotomates_messages', 'fotomates_message', __( 'Settings Saved', 'fotomates' ), 'updated' );
	}

	// show error/update messages
	settings_errors( 'fotomates_messages' );
	?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<form action="options.php" method="post">
			<?php
			// output security fields for the registered setting "fotomates"
			settings_fields( 'fotomates' );
			// output setting sections and their fields
			// (sections are registered for "fotomates", each field is registered to a specific section)
			do_settings_sections( 'fotomates' );
			// output save settings button
			//submit_button( 'Save Settings' );
			?>
		</form>
	</div>
	<?php
}
